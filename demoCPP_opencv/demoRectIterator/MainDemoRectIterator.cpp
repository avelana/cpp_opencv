#include <iostream>
#include <opencv2/opencv.hpp>
//#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

class Aggregate
{
};

class ConcreteAggregate : public Aggregate
{
public:
	Mat img;
	friend class BaseIterator;
	ConcreteAggregate(const Mat &image);
	~ConcreteAggregate();
};

ConcreteAggregate::ConcreteAggregate(const Mat &image) {
	img = image.clone();
}
ConcreteAggregate::~ConcreteAggregate() {
	img.~Mat();
	;
}


class Iterator
{
protected:
	Rect curr_rect_;
public:

	//virtual void first(); // ������������� �������� � ��������� �������
	//virtual bool hasNext(); //�������� ���� �� ��������� �������
	//virtual void next(); // �������� ������� ���������, ������ next()
	//virtual Rect* currentItem(); // ��������� ������ � ������� �������, ������ next()
};



class BaseIterator : public Iterator
{
protected:
	// 1. Design an "iterator" class
	const ConcreteAggregate &fullImage;
	int w_;
	int h_;
public:
	void toString() {
		cout << "ConcreteIterator (Rect): (" << curr_rect_.x << ", " << curr_rect_.y << ", "
			<< curr_rect_.width << ", " << curr_rect_.height << ")\n";
	}
	BaseIterator(const ConcreteAggregate &img, int width, int height) : fullImage(img), w_(width), h_(height) {
		curr_rect_.x = 0;
		curr_rect_.y = 0;
		curr_rect_.width = (fullImage.img.cols > w_) ? w_ : fullImage.img.cols;
		curr_rect_.height = (fullImage.img.rows > h_) ? h_ : fullImage.img.rows;
	}
	Rect& first()
	{
		return curr_rect_;
	}
	bool hasNext()
	{
		if (hasNextHorizontally()) {
			return true;
		}
		if (hasNextVertically()) {
			return true;
		}
		return false;
	}
	Rect& next()
	{
		if (hasNextHorizontally()) {
			curr_rect_.x += curr_rect_.width;
			curr_rect_.width = (fullImage.img.cols > curr_rect_.x + w_) ? w_ : fullImage.img.cols - curr_rect_.x;
			return curr_rect_;
		}
		if (hasNextVertically()) {
			curr_rect_.x = 0;
			curr_rect_.width = (fullImage.img.cols > w_) ? w_ : fullImage.img.cols;
			curr_rect_.y += curr_rect_.height;
			curr_rect_.height = (fullImage.img.rows > curr_rect_.y + h_) ? h_ : fullImage.img.rows - curr_rect_.y;
			return curr_rect_;
		}
		return curr_rect_; // ���� ���������� ���, �� �� ���������, � ���������� �������
	}
	Rect& currentItem()
	{
		return curr_rect_;
	}

protected:
	bool hasNextHorizontally() {
		if (fullImage.img.cols > curr_rect_.x + curr_rect_.width) {
			return true;
		}
		return false;
	}

	bool hasNextVertically() {
		if (fullImage.img.rows > curr_rect_.y + curr_rect_.height) {
			return true;
		}
		return false;
	}
};

class ChessIterator : public BaseIterator {
	bool isOddLine = true; // ������ ������ ��������, �������� ��� ������� 
public:
	ChessIterator(const ConcreteAggregate &img, int width, int height) : BaseIterator(img, width, height) {}
	bool hasNext()
	{
		if (hasNextHorizontally()) {
			return true;
		}
		if (hasNextVertically()) {
			return true;
		}
		return false;
	}
	Rect& next()
	{
		if (hasNextHorizontally()) {
			curr_rect_.x += curr_rect_.width * 2;
			curr_rect_.width = (fullImage.img.cols > curr_rect_.x + w_) ? w_ : fullImage.img.cols - curr_rect_.x;
			return curr_rect_;
		}
		if (hasNextVertically()) {
			isOddLine = !isOddLine;

			if (isOddLine) {
				curr_rect_.x = 0;
				curr_rect_.width = (fullImage.img.cols > w_) ? w_ : fullImage.img.cols;
			}
			else {
				curr_rect_.x = w_;
				curr_rect_.width = (fullImage.img.cols > 2 * w_) ? w_ : fullImage.img.cols - w_;
			}

			curr_rect_.y += curr_rect_.height;
			curr_rect_.height = (fullImage.img.rows > curr_rect_.y + h_) ? h_ : fullImage.img.rows - curr_rect_.y;
			return curr_rect_;
		}
		return curr_rect_; // ���� ���������� ���, �� �� ���������, � ���������� �������
	}

protected:
	// �.�. ��� ����� 2 �������������� � ����� ���������
	bool hasNextHorizontally() {
		if (fullImage.img.cols > curr_rect_.x + 2 * curr_rect_.width) {
			return true;
		}
		return false;
	}

};


//bool operator == (const Stack &l, const Stack &r)
//{
//	// 3. Clients ask the container object to create an iterator object
//   StackIter *itl = new StackIter(&l);//= l.createIterator();
//   StackIter *itr = new StackIter(&r); // r.createIterator();
//	// 4. Clients use the first(), isDone(), next(), and currentItem() protocol
//	for (itl->first(), itr->first(); !itl->isDone(); itl->next(), itr->next())
//		if (itl->currentItem() != itr->currentItem())
//			break;
//	bool ans = itl->isDone() && itr->isDone();
//	delete itl;
//	delete itr;
//	return ans;
//}

int main(char **argv, int argc)
{

	Mat img = imread("../data/opencv-logo.jpg");

	if (img.empty()) {
		cout << "Error loading the image \n";
		system("pause");
		return -1;
	}

	ConcreteAggregate newImage(img);

	// TODO
	/*
		��������, ������� ��������� ������ ��������, ������ ����
		������� ���� ������� �� ���������� ����� � �������

	*/
	const int dw = 10;
	const int dh = 10;

	//// ������� ������������� ������ ��������
	//rectangle(img, currentRect, color);
	//// ���������� �� ����������� ������ ���������� �����
	//for (int i = 0; i < img.rows; i++)
	//	for (int j = 0; j < img.cols; j++)
	//		if ((i % 20 == 10 && j % 2 == 1) || (j % 50 == 25 && i % 2 == 1))
	//		{
	//			img.at<Vec3b>(i, j)[0] = 0;
	//			img.at<Vec3b>(i, j)[1] = 0;
	//			img.at<Vec3b>(i, j)[2] = 0;
	//		}

	Scalar color(0, 255, 0);

	/* ��� �������������� �� �������� */
	//BaseIterator simpleIter(newImage, dw, dh);
	//Rect &currentRect = simpleIter.first();

	//while (simpleIter.hasNext()) {
	//	//simpleIter.toString();
	//	rectangle(img, currentRect, color);
	//	simpleIter.next();
	//}

	/* �������� � ��������� ������� */
	ChessIterator chessIter(newImage, dw, dh);
	Rect &currentRect = chessIter.first();

	while (chessIter.hasNext()) {
		rectangle(img, currentRect, color);
		chessIter.next();
	}

	namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("Display window", img); // Show our image inside it.


	cout << "The end!";

	waitKey(0);
	return 0;
}
